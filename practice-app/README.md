# Practice Spring Boot Java App

This is a very small web service written in Java with Spring Boot.

# Prerequisites

* Java 10+ (see [Oracle Download Page](https://www.oracle.com/technetwork/java/javase/downloads/index.html))
* Maven (see [Maven Download Page](https://maven.apache.org/download.cgi))

# Build

```bash
mvn package
```

# Run

```bash
mvn spring-boot:run
```

Open in browser: http://localhost:8080


# Contribution

You want to contribute code or submit your exercise results. 
But you don't have permission to push into our repo. What do you do?

## Forking Workflow

We follow the Bitbucket standard 
[Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) 
for open source projects.
This means before you start your changes you should fork this repository
(i.e. create your own copy in Bitbucket or Github), 
perform your changes there and then submit a 
[Pull Request](https://www.atlassian.com/git/tutorials/making-a-pull-request).
The steps are explained in the the 
[Atlassian Pull Request Tutorial](https://www.atlassian.com/git/tutorials/making-a-pull-request#example).

We can then review your changes and incorporate them as we see fit.

## Alternative: Git Patch
If you do not want to fork the repo and host it publicly you can also jsut submit your patch as a Git patch file.
For explanation see
[Send A Patch To Someone Using `git format-patch`](https://thoughtbot.com/blog/send-a-patch-to-someone-using-git-format-patch)
and the [Git docs](https://git-scm.com/docs/git-format-patch).

(this may also be easier if you have already cloned the repo and started working on it locally.)
