#Job offer Senior Software Engineer - troy gmbh

##about troy

*We reinvent debt collection!*

We are the customer keepers of the debt collection industry! troy combines decades of debt collection experience with customer experience and optimized recovery through cloud technology in a young fintech startup. We serve the needs of our clients and at the same time keep expensively won customers.
Learn more. https://www.troy-bleiben.de/ueber-troy/

*What you should know about us*

We are convinced that we can make debt collection a positive experience for all customers. We sustain the customer relationship for our clients. This means unconditional friendliness in the collection process. Together, we live strength orientation, openness and transparency. We strive to be better than our competitors. We understand mistakes as a learning opportunity and consistently pursue our goals. We want to have fun at work, promote entrepreneurial thinking, live efficiency and be creative. We protect customer data with the latest methods.
Learn more: https://www.troy-bleiben.de/inkasso/

We work in a team to improve our platform. As a software factory, we use software services and work closely with specialists. As a startup, each of us also assumes responsibility outside the box. We try new things and learn from mistakes.

##Your benefits

* full or part time work with fixed salary
* 30 days of paid vacation per year (for a full time employee) 
* introduction to all parts of the company and personal coaching
* dynamic work environment where you can take responsibility and leverage your own skills 
* participation in meetups and hackathons
* further benefits like fresh fruits and drinks


##Your tasks

There are many possibilities:

###As Backend Developer

In the backend you develop in Java / Kotlin and contribute to the modularization and automation of processes (where useful with machine learning). You help us to implement and improve our ambitious development roadmap.

###As a Team Lead
With your help, we want to set up a development team to build he UI for the customer center and client portal (in React+Redux). You integrate external data sources and APIs with single sign-on and do not shy away from writing JavaScript and frontend tests.

###As CRM Engineer
Or you map our data model to our CRM using REST APIs and work closely with our case managers to simplify workflows in the CRM back office.
Another related task is the team for the communication channels Mail, SMS, Whatsapp, Webchat and Print. Not just sending mails via APIs, but also bounce management. In print integration, you automate the templating and data exchange of PDFs with our print service provider.

###As Data Engineer
Or your team improves the import of customer data and the on-boarding of new clients using data conversion on AWS. The main application has a complex DB model and you have to master SQL. Real data also means getting your fingers dirty with spreadsheets, SFTP, and XML.

##Our Technology Stack
We use Java 10, Kotlin, Sprint Boot, Git, REST APIs, AWS, Linux and Windows, SQL, Shell Scripting, JavaScript and Python for machine learning.

##Your Development Environment
Our development process is trunk-based Monorepo with Git. We have continuous integration and one-click blue-green deploys.
We follow a flexible Scrum process with weekly iterations, planning, backlog grooming, daily standups, and periodical retros. Code reviews have priority. Each ticket is approved together with a subject matter expert. Daily videocalls are normal.
The IT works almost daily together in our Hamburg office. It is new, sunny and has a lot of space; the subway and a shopping street are within walking distance.
You can use your own laptop (if it has hardware encryption and virus scanner) or you can get a brand new development laptop.
We mainly use open source software in the Java VM environment (Java 10, Kotlin, IntelliJ), but if necessary also Java Script (also server side) and shell scripts.

##The Interviews
We want to know more about you and of course answer your questions.
Together we would like to talk about the following topics:

* Introduction,
* Software engineering,
* Agile development methods,
* Team and team lead skills,
* Security mindset,
* Software architecture and
* Next Steps.

Of course we would also like to see your skills developer and how you solve real problems. We invite you to an appointment on site and you get to know the whole team - maybe during a lunch together.



