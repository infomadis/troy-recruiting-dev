# Engineer @ troy

You are interested in an engineering job at troy? 

# Jobs

You can find job descriptions in the [jobs folder](./job-descriptions).

# Demo Application

We have a small example [Spring Boot](https://spring.io/projects/spring-boot) application that we use for coding demos. 

See the [Practice App folder](./practice-app) for details.

We accept pull requests for improvements to show off your proficiency (but please leave space for other contributors).

